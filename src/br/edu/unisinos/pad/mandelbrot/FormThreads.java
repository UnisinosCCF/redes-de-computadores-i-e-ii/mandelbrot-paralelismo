package br.edu.unisinos.pad.mandelbrot;

import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Tela de Form para start de desenho.
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 12 de jun. de 2021
 */
public class FormThreads extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	private JTextField qtdThreadsField;
	private JTextPane painel;

	// valores
	private int escala = 2;
	private int timeoutMaximo = 0;

	public void updatePainelInfo(ThreadClass[] threads) {
		String value = "";
		if (threads != null) {
			for (ThreadClass threadClass : threads) {
				value += "\nID: " + threadClass.getCreateID() + "\tTimeout (s): " + ((int) (threadClass.getTimeout() / 1000)) + "\tSituac�o: "
						+ threadClass.getStatus().toUpperCase();
			}
		}
		this.painel.setText(value);
	}

	public int getEscala() {
		return escala;
	}

	public void setEscala(int escala) {
		this.escala = escala;
	}

	public int getTimeoutMaximo() {
		return timeoutMaximo;
	}

	public void setTimeoutMaximo(int timeoutMaximo) {
		this.timeoutMaximo = timeoutMaximo;
	}

	/**
	 * Colocar o Form na direita da tela.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 12 de jun. de 2021
	 */
	private void setLocationToTopRight() {
		GraphicsConfiguration config = this.getGraphicsConfiguration();
		Rectangle bounds = config.getBounds();
		Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(config);
		int x = bounds.x + bounds.width - insets.right - this.getWidth();
		int y = bounds.y + insets.top;
		this.setLocation(x, y);
	}

	/**
	 * Create the frame.
	 */
	public FormThreads(Main main) {
		setResizable(false);
		setTitle("PAD 2021/01 - Mandelbrot + Palalelismo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 665, 701);
		mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(mainPanel);

		setLocationToTopRight();
		mainPanel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Defina a Escala:");
		lblNewLabel.setBounds(27, 94, 114, 14);
		mainPanel.add(lblNewLabel);

		JSpinner spinner = new JSpinner();
		spinner.setBounds(131, 91, 45, 20);
		spinner.getValue();
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				int value = (int) spinner.getValue();
				qtdThreadsField.setText((value * value) + "");
				escala = value;
			}
		});
		spinner.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				int value = (int) spinner.getValue();
				qtdThreadsField.setText((value * value) + "");
				escala = value;
			}
		});
		spinner.setModel(new SpinnerNumberModel(new Integer(2), new Integer(2), null, new Integer(1)));
		mainPanel.add(spinner);

		JLabel lblNmeroDeThreads = new JLabel("Qtd de Threads:");
		lblNmeroDeThreads.setBounds(27, 130, 114, 14);
		mainPanel.add(lblNmeroDeThreads);

		qtdThreadsField = new JTextField();
		qtdThreadsField.setBounds(131, 127, 45, 20);
		qtdThreadsField.setText("4");
		qtdThreadsField.setEditable(false);
		mainPanel.add(qtdThreadsField);
		qtdThreadsField.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Mandelbrot");
		lblNewLabel_1.setBounds(10, 11, 337, 48);
		lblNewLabel_1.setFont(new Font("Arial Black", Font.PLAIN, 42));
		mainPanel.add(lblNewLabel_1);

		JLabel lblTimeoutMximo = new JLabel("Timeout M\u00E1ximo:");
		lblTimeoutMximo.setBounds(199, 94, 114, 14);
		mainPanel.add(lblTimeoutMximo);

		JSpinner spinnerTime = new JSpinner();
		spinnerTime.setBounds(304, 91, 45, 20);
		spinnerTime.setModel(new SpinnerNumberModel(0, 0, 100, 1));
		spinnerTime.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				int value = (int) spinnerTime.getValue();
				timeoutMaximo = value;
			}
		});
		mainPanel.add(spinnerTime);

		JLabel lblSegundos = new JLabel("segundos");
		lblSegundos.setBounds(359, 94, 114, 14);
		mainPanel.add(lblSegundos);

		JToggleButton start = new JToggleButton("Iniciar");
		start.setBounds(533, 94, 99, 55);
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				start.setEnabled(false);
				spinner.setEnabled(false);
				spinnerTime.setEnabled(false);
				main.start(escala, timeoutMaximo);
			}
		});
		mainPanel.add(start);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 185, 608, 476);
		mainPanel.add(scrollPane);

		painel = new JTextPane();
		scrollPane.setViewportView(painel);
		painel.setEditable(false);
	}
}
