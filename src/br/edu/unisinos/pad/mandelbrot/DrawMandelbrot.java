package br.edu.unisinos.pad.mandelbrot;

import java.awt.Canvas;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;

@SuppressWarnings("serial")
public class DrawMandelbrot extends Canvas {
	// size of fractal in pixels (HEIGHT X HEIGHT)
	private final int height;
	// how long to test for orbit divergence
	private final int NUM_ITERATIONS = 50;
	private Frame frame;

	private int colorscheme[];
	private Image img;

	// 2-dimensional array of colors stored in packed ARGB format
	private int[] fractal;

	public DrawMandelbrot(int height, Frame frame) {
		this.height = height;
		this.frame = frame;
		this.frame.setSize(height, height);
		this.frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

	}

	public void start() {
		this.colorscheme = new int[NUM_ITERATIONS + 1];
		// fill array with color palette going from Red over Green to Blue
		int scale = (255 * 2) / NUM_ITERATIONS;

		// going from Red to Green
		for (int i = 0; i < (NUM_ITERATIONS / 2); i++)
			// Alpha=255 | Red | Green | Blue=0
			colorscheme[i] = 0xFF << 24 | (255 - i * scale) << 16 | i * scale << 8;

		// going from Green to Blue
		for (int i = 0; i < (NUM_ITERATIONS / 2); i++)
			// Alpha=255 | Red=0 | Green | Blue
			colorscheme[i + NUM_ITERATIONS / 2] = 0xFF000000 | (255 - i * scale) << 8 | i * scale;

		// convergence color
		colorscheme[NUM_ITERATIONS] = 0xFF0000FF; // Blue

		this.fractal = new int[(height) * (height)];
		frame.add(this);
		frame.setVisible(true);
	}

	@Override
	public void paint(Graphics graphics) {
		graphics.drawImage(this.img, 0, 0, null);
	}

	/**
	 * Auxiliary function that converts an array of pixels into a BufferedImage.
	 * This is used to be able to quickly draw the fractal onto the canvas using
	 * native code, instead of us having to manually plot each pixel to the canvas.
	 */
	public void updateImage() {
		// RGBdefault expects 0x__RRGGBB packed pixels
		ColorModel cm = DirectColorModel.getRGBdefault();
		SampleModel sampleModel = cm.createCompatibleSampleModel(height, height);
		DataBuffer db = new DataBufferInt(fractal, height, 0);
		WritableRaster raster = Raster.createWritableRaster(sampleModel, db, null);
		this.img = new BufferedImage(cm, raster, false, null);
		frame.add(this);
		frame.setVisible(true);
	}

	/**
	 * M�todo utilizado para desenhar uma parte do mandelbrot.
	 * 
	 * @since 10 de jun. de 2021
	 * @param id
	 * @param srcx
	 * @param srcy
	 * @param size
	 */
	public void draw(int id, int srcx, int srcy, int size) {
		double x, y, t, cx, cy;
		int k;

		// loop over specified rectangle grid
		for (int px = srcx; px < srcx + size; px++) {
			for (int py = srcy; py < srcy + size; py++) {
				x = 0;
				y = 0;
				// convert pixels into complex coordinates between (-2, 2)
				cx = (px * 4.0) / height - 2;
				cy = 2 - (py * 4.0) / height;
				// test for divergence
				for (k = 0; k < NUM_ITERATIONS; k++) {
					t = x * x - y * y + cx;
					y = 2 * x * y + cy;
					x = t;
					if (x * x + y * y > 4)
						break;
				}
				fractal[px + height * py] = colorscheme[k];
			}
		}
	}
}
