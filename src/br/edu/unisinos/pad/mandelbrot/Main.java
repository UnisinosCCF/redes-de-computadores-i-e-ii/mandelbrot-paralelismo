package br.edu.unisinos.pad.mandelbrot;

import java.awt.Frame;
import java.util.Random;

public class Main {

	private static final Random random = new Random();
	private FormThreads frameForm;
	private ThreadClass[] threads;
	public static int THREADS_FINALIZADAS = 0;

	public Main() {
		this.frameForm = new FormThreads(this);
		frameForm.setVisible(true);
	}

	// private static void shuffle(ThreadClass[] threads) {
	// for (int i = 0; i < (threads.length - 1); i++) {
	// // sorteia um �ndice
	// int j = random.nextInt(threads.length);
	// // troca o conte�do dos �ndices i e j do vetor
	// ThreadClass temp = threads[i];
	// threads[i] = threads[j];
	// threads[j] = temp;
	// }
	// }

	private static ThreadClass[] createThreads(int escalaMatriz, int height, int maximoTimeout, DrawMandelbrot mandelbrot) {
		final int y_max = escalaMatriz;
		final int x_max = escalaMatriz;
		final int max_threads = x_max * y_max;
		ThreadClass[] threads = new ThreadClass[max_threads];

		int y = 0;
		int x = 0;
		for (int j = 0; j < threads.length; j++) {
			int timeout = (maximoTimeout > 0 ? random.nextInt(maximoTimeout * 1000) : 0);
			threads[j] = new ThreadClass((j + 1), ((x++ * height) / x_max) - (x - 1), ((y * height) / y_max) - (y - 1), (height / y_max), timeout, mandelbrot);
			if (x == x_max) {
				x = 0;
				y++;
			}
		}
		return threads;
	}

	public void start(int escala, int maximoTimeoutRandom) {
		// Mandebrolt
		int height = 1000;
		Frame frame = new Frame();
		DrawMandelbrot drawMandebrolt = new DrawMandelbrot(height, frame);

		// Start
		drawMandebrolt.start();

		threads = createThreads(escala, height, maximoTimeoutRandom, drawMandebrolt);

		// embaralha as threads
		// shuffle(threads);

		// Inicia as threads
		for (ThreadClass thread : threads) {
			thread.start();
		}
	}

	/**
	 * Fazer aqui metodo que ira verificar as threads e atualizar o frame
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 12 de jun. de 2021
	 * @throws InterruptedException
	 */
	public void verificarThreads() throws InterruptedException {
		// THREADS_FINALIZADAS >= threads.length
		while (true) {
			System.out.println("Thread Main\t-> " + THREADS_FINALIZADAS + "/" + (threads == null ? 0 : threads.length));
			this.frameForm.updatePainelInfo(threads);
			Thread.sleep(2000);
		}

	}

	/**
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 12 de jun. de 2021
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		Main main = new Main();
		main.verificarThreads();
	}
}
