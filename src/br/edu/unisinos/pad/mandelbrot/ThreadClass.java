package br.edu.unisinos.pad.mandelbrot;

public class ThreadClass extends Thread {
	private int id;
	private int srcx;
	private int srcy;
	private int size;
	private DrawMandelbrot drawMandelbrot;
	private int timeout;
	private String status = "Criada";

	public ThreadClass(int id, int x, int y, int size, int timeout, DrawMandelbrot mandelbrot) {
		this.id = id;
		this.srcx = x;
		this.srcy = y;
		this.size = size;
		this.drawMandelbrot = mandelbrot;
		this.timeout = timeout;
	}

	public int getTimeout() {
		return timeout;
	}

	public String getStatus() {
		return status;
	}

	public int getCreateID() {
		return this.id;
	}

	@Override
	public void run() {
		this.status = "Iniciada";
		try {
			Thread.sleep(timeout);
			this.status = "Calculando";
			drawMandelbrot.draw(0, srcx, srcy, size);
			drawMandelbrot.updateImage();
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.status = "Finalizada";
		System.out.println("Thread " + this.getId() + "\t-> Encerrada...");
		Main.THREADS_FINALIZADAS++;

	}
}